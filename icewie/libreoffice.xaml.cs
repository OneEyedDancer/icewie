﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace icewie
{
    public sealed partial class libreoffice : Page
    {
        public libreoffice()
        {
            this.InitializeComponent();
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Запуск ссылки
            await Launcher.LaunchUriAsync(new Uri("https://www.libreoffice.org/download/download/?lang=ru"));
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: Windows, Linux, Mac OS
Процессор: Pentium III, Athlon и более новые системы
Оперативная память: 256 MB ОЗУ
Место на диске: До 1,5 GB",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
            }
        }
}
