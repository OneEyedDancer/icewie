﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace icewie
{
    public sealed partial class gimp : Page
    {
        public gimp()
        {
            InitializeComponent();
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Запуск ссылки
            await Launcher.LaunchUriAsync(new Uri("https://www.gimp.org/downloads/"));
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: Windows XP, 7, Vista, 10, Linux
Процессор: Pentium IV, от 700 MHz
Место на диске: 100 MB",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
            }
        }
}
