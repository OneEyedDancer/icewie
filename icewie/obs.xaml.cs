﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;


namespace icewie
{
    public sealed partial class obs : Page
    {
        public obs()
        {
            InitializeComponent();
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Запуск ссылки
            await Launcher.LaunchUriAsync(new Uri("https://obsproject.com/ru"));
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: Windows, Linux, Mac OS
Процессор: Intel Pentium Gold G5400
Оперативная память: 16 GB ОЗУ
Видеокарта: GeForce GTX 1050Ti или AMD RX 570
Место на диске: 100 MB",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
            }
        }
}
