﻿using System;
using Windows.System;
using Windows.UI.Xaml.Controls;

namespace icewie
{
    public sealed partial class home : Page
    {
        public home()
        {
            InitializeComponent();
        }

        private async void Button_Click_1(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: Windows, Linux
Процессор: от 1.5 ГГц
Оперативная память: 512 MB ОЗУ
Место на диске: 250 MB",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
        }

        private async void Button_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            // Запуск ссылки
            await Launcher.LaunchUriAsync(new Uri("https://codeberg.org/OneEyedDancer/icewie"));
        }
    }
}
