﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace icewie
{
    public sealed partial class kdenlive : Page
    {
        public kdenlive()
        {
            InitializeComponent();
        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // открытие ссылки в браузере
            await Launcher.LaunchUriAsync(new Uri("https://kdenlive.org/en/download/"));
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e) // должен быть async
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: 64-bit Windows 7 or newer, Apple macOS 10.14 or newer, 64-bit Linux
Процессор: 4 cores for HD-Video
Оперативная память: 8 GB ОЗУ
Место на диске: 100 MB",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
        }
    }
}
