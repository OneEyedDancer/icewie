﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace icewie
{
    public sealed partial class freecad : Page
    {
        public freecad()
        {
            InitializeComponent();
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Запуск ссылки
            await Launcher.LaunchUriAsync(new Uri("https://www.freecadweb.org/?lang=ru"));
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: Windows, Linux, Mac OS
Процессор: Pentium 1 ГГц
Оперативная память: 512 MB ОЗУ
Место на диске: 500 MB",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
        }
    }
}
