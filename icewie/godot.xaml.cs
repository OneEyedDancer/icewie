﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace icewie
{

    public sealed partial class godot : Page
    {
        public godot()
        {
            InitializeComponent();
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Запуск ссылки
            await Launcher.LaunchUriAsync(new Uri("https://godotengine.org/download"));
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: Windows, Mac OS, Linux
Видеокарта: OpenGL 2.1",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
        }
    }
}
