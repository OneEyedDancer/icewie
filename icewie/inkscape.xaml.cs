﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace icewie
{

    public sealed partial class inkscape : Page
    {
        public inkscape()
        {
            InitializeComponent();
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Запуск ссылки
            await Launcher.LaunchUriAsync(new Uri("https://inkscape.org/release/inkscape-1.1.2/"));
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: Windows, Linux
Процессор: от 1.5 ГГц
Оперативная память: 512 MB ОЗУ
Место на диске: 250 MB",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
        }
    }
}
