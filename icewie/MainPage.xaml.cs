﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace icewie
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();

            // по умолчанию открываем страницу home.xaml
            myFrame.Navigate(typeof(home));
            TitleTextBlock.Text = "Главная";
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (home.IsSelected)
            {
                myFrame.Navigate(typeof(home));
                TitleTextBlock.Text = "Главная";
            }
            else if (blender.IsSelected)
            {
                myFrame.Navigate(typeof(blender));
                TitleTextBlock.Text = "Maya";
            }
            else if (kdenlive.IsSelected)
            {
                myFrame.Navigate(typeof(kdenlive));
                TitleTextBlock.Text = "VEGAS Pro";
            }
            else if (gimp.IsSelected)
            {
                myFrame.Navigate(typeof(gimp));
                TitleTextBlock.Text = "Adobe Photoshop";
            }
            else if (libreoffice.IsSelected)
            {
                myFrame.Navigate(typeof(libreoffice));
                TitleTextBlock.Text = "MS Word";
            }
            else if (obs.IsSelected)
            {
                myFrame.Navigate(typeof(obs));
                TitleTextBlock.Text = "Camtasia";
            }
            else if (godot.IsSelected)
            {
                myFrame.Navigate(typeof(godot));
                TitleTextBlock.Text = "Unity";
            }
            else if (inkscape.IsSelected)
            {
                myFrame.Navigate(typeof(inkscape));
                TitleTextBlock.Text = "Adobe Illustrator";
            }
            else if (freecad.IsSelected)
            {
                myFrame.Navigate(typeof(freecad));
                TitleTextBlock.Text = "Autodesk AutoCAD";
            }
            else if (qbittorrent.IsSelected)
            {
                myFrame.Navigate(typeof(qbittorrent));
                TitleTextBlock.Text = "uTorrent";
            }
            else if (sevenzip.IsSelected)
            {
                myFrame.Navigate(typeof(sevenzip));
                TitleTextBlock.Text = "WinRAR";
            }
        }

        private void HamburgerButton_Click(object sender, RoutedEventArgs e)
        {
            mySplitView.IsPaneOpen = !mySplitView.IsPaneOpen;
        }

        private async void About_Click(object sender, RoutedEventArgs e)
        {
            ContentDialog aboutDialog = new ContentDialog()
            {
                Title = "О Icewie",
                Content = @"Создали:
    NotYourPie
    LinInFal
    OneEyedDancer

Лицензия: GPL3",
                CloseButtonText = "OK"
            };

            await aboutDialog.ShowAsync();
        }
    }
}
