﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace icewie
{

    public sealed partial class sevenzip : Page
    {
        public sevenzip()
        {
            InitializeComponent();
        }
        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            // Запуск ссылки
            await Launcher.LaunchUriAsync(new Uri("https://www.7-zip.org/download.html"));
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ContentDialog noWifiDialog = new ContentDialog()
            {
                Title = "Системные требования",
                Content = @"ОС: Windows, Linux, Mac OS
Процессор: Intel/AMD
Оперативная память: 64 MB ОЗУ
Место на диске: 3 MB",
                CloseButtonText = "OK"
            };

            await noWifiDialog.ShowAsync();
        }
    }
}
